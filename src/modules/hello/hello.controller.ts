import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { HelloService } from './hello.service';

@ApiTags('hello')
@Controller('hello')
export class HelloController {
  constructor(private readonly helloService: HelloService) {
  }

  @ApiOkResponse({ description: 'Returns hello world', schema: { example: "Hello World!" } })
  @Get()
  getHello(): Object {
    return this.helloService.getHello();
  }
}
