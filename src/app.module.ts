import { Module } from '@nestjs/common';
import { HelloModule } from './modules/hello/hello.module';
import { HealthcheckModule } from './modules/healthcheck/healthcheck.module';
import { AuthModule } from './modules/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './modules/users/users.module';
import { Users } from './modules/users/entities/user.entity';

@Module({
  imports: [HelloModule, HealthcheckModule, AuthModule, TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'demo',
    password: 'demo',
    database: 'demo',
    entities: [Users],
    synchronize: true,
  }), UsersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
