import { Exclude } from "class-transformer";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  email: string

  @Exclude()
  @Column()
  password: string

  @Column({ default: true })
  isActive: boolean;

  constructor(partial: Partial<Users>) {
    Object.assign(this, partial);
  }
}
