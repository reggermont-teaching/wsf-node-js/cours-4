import * as bcrypt from "bcrypt";

export function hashPassword(plainPassword: string) {
  const salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(plainPassword, salt);
}

