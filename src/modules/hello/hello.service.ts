import { Injectable } from '@nestjs/common';

@Injectable()
export class HelloService {
  getHello(): Object {
    return { message: 'Hello World!' };
  }
}
