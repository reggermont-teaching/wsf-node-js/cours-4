import { Users } from './entities/user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { hashPassword } from '../../lib/bcrypt';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
  ) { }

  create(createUserDto: CreateUserDto) {
    const password = hashPassword(createUserDto.password);

    const newUser = this.usersRepository.create({ ...createUserDto, password });

    return this.usersRepository.save(newUser);

  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: string) {
    return this.usersRepository.findBy({ "id": id });
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: string) {
    return this.usersRepository.delete({ "id": id })
  }
}
